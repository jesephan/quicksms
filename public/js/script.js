(function($){
	hideErrorMsg();
	$("#receiver").select2();
	$('#send-sms').on('click', function(e){
		e.preventDefault();
		var receivers = $('#receiver').val();
		var message = $('#message').val();
		var error_msg = '';
		if( receivers == null ||  message.length <= 0) {
			error_msg += receivers == null ? '<li>Receiver is required</li>' : '';
			error_msg += message.length <= 0 ? '<li>Message is required</li>' : '';
			inputError(error_msg, $('#sms-error-msg'));
			return false;
		}
		swal({
				title: "Send the message now?",
				text: "You are about to send an SMS.",
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#128f76",
				confirmButtonText: "Yes!",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var form        = $('form');
				var param 		= {};
				param.url       = '/profile/send';
				param.method    = 'POST';
				param.data 		= $('#send-sms-form').serialize();
				var result = send(param);
				result.done(function(response){
					if(response['code'] == 200){
						displayAlert('Success!','Message was sent.', 'success');
						$('#message').val('');
						$("#receiver").select2().select2('val','');
					} else {
						displayAlert('Failed!','Error sending the message.', 'info', true);;
					}
				}).fail(function(){
					displayAlert('Failed!','Server error.', 'info', true);;
				});
			});
	})

	$('#invite-new-user').on('click','#invite-contact-btn', function(e){
		e.preventDefault();
		$('#admin-invite-contact-modal-error-msg').html('');
		var email = $('#invite-email').val();
		var isValidEmail = validateEmail(email);
		var error_msg = '';
		if(email.length <= 0) {
			error_msg += email.length <= 0 ? '<li>Email is required</li>' : '';
			inputError(error_msg, $('#admin-invite-contact-modal-error-msg'));
			return false;
		}
		if(!isValidEmail) {
			error_msg += !isValidEmail ? '<li>Email is invalid</li>' : '';
			inputError(error_msg, $('#admin-invite-contact-modal-error-msg'));
			return false;
		}
		swal({
				title: "Please confirm",
				text: "You are about to invite a new user.",
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes!",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var form        = $('form');
				var param 		= {};
				param.url       = '/profile/invite';
				param.method    = 'POST';
				param.data 		= $('#invite-new-user-form').serialize();
				var result = send(param);
				result.done(function(response){
					if(response['code'] == 200){
						displayAlert('Success!','New User was invited.', 'success');
						$('#invite-email').val('');
					}else if(response['code'] == 406){
						displayAlert('Failed!','The email address is already invited.', 'info', true);
					} else {
						displayAlert('Failed!','Error inviting the contact.', 'info', true);;
					}
				}).fail(function(){
					displayAlert('Failed!','Server error.', 'info', true);;
				});
			});
	})

	$('#edit-profile').on('click','#edit-profile-btn', function(e){
		e.preventDefault();
		$('#edit-profile-modal-error-msg').html('');
		var name = $('#edit-profile-name').val();
		var phone_number = $('#edit-profile-phone-number').val();
		var error_msg = '';
		if( name.length <= 0 ||  phone_number.length <= 0) {
			error_msg += name.length <= 0 ? '<li>Name is required</li>' : '';
			error_msg += phone_number.length <= 0 ? '<li>Phone Number is required</li>' : '';
			inputError(error_msg, $('#edit-profile-modal-error-msg'));
			return false;
		}

		swal({
				title: "Are you sure?",
				text: "You are about to change your profile.",
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes!",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var param 		= {};
				param.url       = '/profile/update';
				param.method    = 'POST';
				param.data 		= $('#edit-profile-form').serialize();
				var result = send(param);
				result.done(function(response){
					if(response['code'] == 200){
						displayAlert('Success!','Contact Updated Successfully.', 'success');
						updateContactOptions();
					} else {
						displayAlert('Failed!','Error adding the contact.', 'info', true);;
					}
				}).fail(function(){
					displayAlert('Failed!','Server error.', 'info', true);;
				});
			});
	})

	$('#contact-list').on('click','#admin-edit-contact-btn', function(e){
		e.preventDefault();
		$('#admin-edit-contact-modal-error-msg').html('');

		var contact_id = $('#admin-edit-contact-id').val();
		var name = $('#admin-edit-name').val();
		var phone_number = $('#admin-edit-phone-number').val();
		var error_msg = '';
		if( name.length <= 0 ||  phone_number.length <= 0) {
			error_msg += name.length <= 0 ? '<li>Name is required</li>' : '';
			error_msg += phone_number.length <= 0 ? '<li>Phone Number is required</li>' : '';
			inputError(error_msg, $('#admin-edit-contact-modal-error-msg'));
			return false;
		}

		swal({
				title: "Are you sure?",
				text: "You are about to change the contact details of this person.",
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes!",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var param 		= {};
				param.url       = '/profile/update';
				param.method    = 'POST';
				param.data 		= $('#edit-contact-form').serialize();
				var result = send(param);
				result.done(function(response){
					if(response['code'] == 200){
						$('#row-'+contact_id).find('.contact-name').text(name);
						$('#row-'+contact_id).find('.contact-phone-number').text(phone_number);
						displayAlert('Success!','Contact Updated Successfully.', 'success');
						$('#admin-edit-name').val('');
						$('#admin-edit-phone-number').val('');
						$('#admin-edit-contact-id').val('');
						$('#edit-contact-form').toggle();
						updateContactOptions();
					}else if(response['code'] == 401) {
						displayAlert('Failed!', 'You are not allowed to update this contact.', 'info', true);;
					}else {
						displayAlert('Failed!','Error updating the contact.', 'info', true);;
					}
				}).fail(function(){
					displayAlert('Failed!','Server error.', 'info', true);;
				});
			});
	})


	$('#change-password').on('click','#change-password-btn', function(e){
		e.preventDefault();
		$('#change-password-modal-error-msg').html('');
		var new_password = $('#new-password').val();
		var confirm_password = $('#confirm-new-password').val();
		var same = new_password == confirm_password;
		var error_msg = '';
		if(
			new_password.length <= 0 ||
			!same
		) {
			error_msg += new_password.length <= 0 ? '<li>New Password is required</li>' : '';
			error_msg += !same ? '<li>Incorrect confirm password</li>' : '';
			inputError(error_msg, $('#change-password-modal-error-msg'));
			return false;
		}

		swal({
				title: "Are you sure?",
				text: "You are about to change your login password.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes! Change it.",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var param 		= {};
				param.url       = '/profile/changePassword';
				param.method    = 'POST';
				param.data 		= $('#change-password-form').serialize();
				var result = send(param);
				result.done(function(response){
					if(response['code'] == 200){
						displayAlert('Success!','New password was set Successfully.', 'success');
						$('#old-password').val('');
						$('#new-password').val('');
						$('#confirm-new-password').val('');
					}else if(response['code'] == 401){
						displayAlert('Failed!','Incorrect old password.', 'info', true);
					} else {
						displayAlert('Failed!','Error changing the password.', 'info', true);;
					}
				}).fail(function(){
					displayAlert('Failed!','Server error.', 'info', true);;
				});
			});

	});

	$('#app-settings').on('click','#edit-app-settings-btn', function(e){
		e.preventDefault();
		$('#app-settings-modal-error-msg').html('');
		var appDeviceId = $('#app_setting_deviceId').val();
		var appEmail = $('#app_setting_email').val();
		var appPassword = $('#app_setting_password').val();
		var isValidEmail = validateEmail(appEmail);
		var error_msg = '';
		if(
			appDeviceId.length <= 0 ||
			appPassword.length <= 0 ||
			appEmail.length <= 0 ||
			!isValidEmail
		) {
			error_msg += appDeviceId.length <= 0 ? '<li>Device ID is required</li>' : '';
			error_msg += appPassword.length <= 0 ? '<li>Password is required</li>' : '';
			error_msg += appEmail.length <= 0 ? '<li>Email is required</li>' : '';
			error_msg += !isValidEmail ? '<li>Invalid Email</li>' : '';
			inputError(error_msg, $('#app-settings-modal-error-msg'));
			return false;
		}

		swal({
				title: "Are you sure?",
				text: "Click Yes if you know what you are doing.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes!",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var param 		= {};
				param.url       = '/profile/updateAppSettings';
				param.method    = 'POST';
				param.data 		= $('#app-settings-form').serialize();
				var result = send(param);
				result.done(function(response){
					if(response['code'] == 200){
						displayAlert('Success!','App Settings Updated Successfully.', 'success');
					} else {
						displayAlert('Failed!','Error updating the App Settings.', 'info', true);;
					}
				}).fail(function(){
					displayAlert('Failed!','Server error.', 'info', true);;
				});
			});
	})

	$('#contacts-table').on('click', '.edit-contact', function(e){
		e.preventDefault();

		$('#admin-edit-contact-id').val($(this).data('edit'));
		$('#edit-contact-form').show();
		$btn = $(this).parent().parent();
		$('#admin-edit-name').val($btn.find('.contact-name').text());
		$('#admin-edit-phone-number').val($btn.find('.contact-phone-number').text());
	})

	$('#contacts-table').on('click', '.delete-contact', function(e){
		e.preventDefault();
		var href = '/profile/delete';
		var user_id = $(this).data('delete');
		swal({
				title: "Are you sure?",
				text: "You will not be able to recover this contact!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
			function(){
				var param = {};
				param.url = href;
				param.method = 'POST';

				$.ajax({
					url: param.url,
					method: param.method,
					data: {'_token': $('input[name="_token"]').val(), 'user_id': user_id}
				}).done(function(response){
					if(response['code'] == 200) {
						$('#row-'+user_id).remove();
						displayAlert('Succes!', 'Contact Deleted', 'success');
						updateContactOptions();
					}else if(response['code'] == 401) {
						displayAlert('Failed!', 'You are not allowed to delete this contact.', 'info', true);
					} else {
						displayAlert('Failed!', 'Error deleting the contact.', 'info', true);
					}
				}).fail(function(){
					displayAlert('Failed!', 'Server Error.', 'info', true);
				});
			});
	})

	function displayAlert(title, msg, type, confirm)
	{
		msg = msg ? msg : "Reqest complete.";
		title = title ? title :'Success!';
		type = type ? type : 'success';
		confirm = confirm ? true : false;
		if(confirm) {
			return swal({
				showConfirmButton: true,
				animation: "slide-from-top",
				title: title,
				text: msg,
				type: type

			});
		}

		return swal({
			showConfirmButton: false,
			animation: "slide-from-top",
			title: title,
			text: msg,
			type: type,
			timer: 2000
		});

	}

	function send(param) 
	{
		if(param.btn) {
			if(param.btn) {
				param.btn.html('Add <i class="fa fa-spinner fa-spin"></i>');
			}
		}
		return $.ajax({
			type: 	param.method,
			url: 	param.url,
			data: 	param.data
		}).promise();
	}	

	function inputError(msg, $el)
	{
	var error = '<div class="alert alert-danger">';
		error += '<strong>Whoops!</strong> There were some problems with your input.<br><br>';
		error += '<ul>';
		error += msg;
		error += '</ul>';
		error += '</div>';
		$el.html(error);
		hideErrorMsg();
	}

	function updateContactOptions()
	{
		var param = {};
		param.url = "/profile/getContacts";
		param.method = "GET";
		param.data = '';

		var result = send(param);

		result.done(function(data){
			$('.select-multiple').empty();
			var $element = $(".select-multiple").select2({
				maximumSelectionLength: 3,
				tags: "true",
				placeholder: "Select Contact(s)",
				allowClear: true
			});
			for (var d = 0; d < data.length; d++) {
				var item = data[d];

				// Create the DOM option that is pre-selected by default
				var option = new Option(item.text, item.id, true);

				// Append it to the select
				$element.append(option);
			}
			$element.trigger('change');
		}).fail(function(){
			displayAlert('Failed!', 'Unable to get the updated contacts list.', 'info', true);
		});

	}

	$('#contact-list-items').on('click', '.send-sms', function(e){
		e.preventDefault();
		var contact = $(this).data('sms');
		$("#receiver").select2().select2('val',contact);
		$('#contact-list').modal('hide');
	})

	function validateEmail(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	}
	function hideErrorMsg()
	{
		setTimeout(function(){
			$('.alert').hide(1000);
		}, 10000);
	}
})(jQuery);