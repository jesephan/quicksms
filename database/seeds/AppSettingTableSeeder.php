<?php

use App\AppSetting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
class AppSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_settings')->delete();
        AppSetting::insert([
            ['tag' => 'email','value' => 'jesusephan@gmail.com'],
            ['tag' => 'password','value' => '57d7261239ad68313ea3f033fe34d829d50038ec'],
            ['tag' => 'deviceId','value' => 'iFpmabK8tIM-XNRXISShUSQGx9P6jFEzgczNskcZQ8'],
        ]);
    }
}
