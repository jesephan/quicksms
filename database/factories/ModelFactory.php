<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'role'      => 'admin',
        'email'     => 'admin@quicksms.com',
        'password'  => bcrypt('password1234'),
        'active'    => 1,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'user_id'       => 1,
        'name'          => 'admin',
        'phone_number'  => '+639499390001',
    ];
});
