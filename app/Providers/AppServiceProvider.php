<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\SmsGateway\Textlocal;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\SmsGateway\TextLocal', function ($app) {
            return new Textlocal();
        });
    }
}
