<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    protected  $fillable = ['email', 'password'];

	public function scopeGetSmsGatewayCredential($query, $tag)
	{
		return $query->where('tag', $tag)->first()->value;
	}
}
