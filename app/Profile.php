<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['user_id', 'name', 'phone_number'];

    public function scopeGetPhoneNumber($query, $profile_id)
    {
        return $query->where('id', $profile_id)->first()->phone_number;
    }
}
