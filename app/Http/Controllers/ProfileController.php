<?php

namespace App\Http\Controllers;

use App\AppSetting;
use App\Events\UserWasInvited;
use App\Http\Requests\AppSettingRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\InviteNewUserRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\SmsRequest;
use App\Http\Requests\UpdateContactRequest;
use App\Profile;
use App\SmsGateway\Textlocal;
use App\User;
use Illuminate\Http\Request;
use App\SmsGateway\SmsGateway;
use App\Http\Requests;
use Illuminate\Auth\Access\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
class ProfileController extends Controller
{
    /**
     *  Display the user dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $current_user_profile   = Profile::where('user_id',Auth::user()->id)->firstorfail();
        $app_setting_email      = AppSetting::GetSmsGatewayCredential('email');
        $app_setting_deviceId   = AppSetting::GetSmsGatewayCredential('deviceId');
        $app_setting_password   = AppSetting::GetSmsGatewayCredential('password');
        $contacts               = $this->getUsers();

        return View('profile.dashboard',
            compact(
                'contacts','current_user_profile',
                'app_setting_password', 'app_setting_email',
                'app_setting_deviceId'
            )
        );
    }

    /**
     *  Get all profiles except the profile of the logged in user.
     *
     * @return mixed
     */
    public function getUsers()
    {
        $active_users   = User::where('active', 1)->where('id', '!=', Auth::user()->id)->lists('id');
        $contacts       = Profile::whereIn('user_id', $active_users)
                            ->where('phone_number','!=', '')
                            ->orderBy('name')
                            ->get(['id','user_id','name', 'phone_number']);

        return $contacts;
    }

    /**
     * Profile Update
     * @param ProfileRequest $request
     * @return mixed
     */
    public function update(ProfileRequest $request)
    {
        // Only admin can update other profile.
        if(!$this->isAdmin()) {
            return Response::json(['code' => 401]);
        }
        
        $profile = Profile::find($request['id']);
        $result =  $profile->update($request->all());

        return Response::json(['code' => 200]);

    }

    /**
     * Save and send new user invitation.
     *
     * @param InviteNewUserRequest $request
     * @return mixed
     */
    public function invite(InviteNewUserRequest $request)
    {
        $user = User::where('email', $request->email)->get();
        if(! $user->isEmpty()) {
            return Response::json(['code' => 406]);
        }
        // Generate temporary password and activation code.
        $temp_password = str_random(10);
        $activation_code = str_random(50).$request->email;

        // Insert new user to the database.
        $user = new User();
        $user->role = 'subscriber';
        $user->email = $request->email;
        $user->password = hash::make($temp_password);
        $user->activation_code = $activation_code;
        $save = $user->save();

        $data = array(
            'password' => $temp_password,
            'code' => $activation_code,
            'email' => $request->email,
        );

        // Fire an event that will send an email to the invited user.
        Event::Fire(new UserWasInvited($data));

        return Response::json(['code' => 200]);

    }

    /**
     * Delete a contact/user.
     *
     * @param Request $request
     * @return mixed
     */
    public function delete(Request $request)
    {
        // Only admin can delte a contact or user.
        if(!$this->isAdmin()) {
            return response::json(['code' => 401]);
        }

        User::find($request->user_id)->delete();
        return response::json(['code' => 200]);
    }

    /**
     * Get all valid contacts from the database.
     *
     * @return mixed
     */
    public function getContacts()
    {
        $contacts = $this->getUsers();
        $allContacts = array();

        foreach($contacts as $contact){
            $allContacts[] = array('id' => $contact->id, 'text' => $contact->name);
        }
        return response::json($allContacts);
    }

    /**
     * Send an SMS.
     *
     * @param SmsRequest $request
     * @param Textlocal $sms
     * @return mixed
     * @throws \App\SmsGateway\Exception
     */
    public function send(SmsRequest $request, Textlocal $sms)
    {
        $receiver_ids = $request->receiver;
        $message = $request->message;
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        foreach($receiver_ids as $id) {
            $number = array(Profile::GetPhoneNumber($id));
            $result = $sms->sendSms($number, $message, $profile->name);
            if($result->status == "success") {
                return response::json(['code'=> 200]);
            }
        }
        return response::json(['code' => 405]);

    }

    /**
     * Update SMS Gateway API credentials.
     *
     * @param AppSettingRequest $request
     * @return mixed
     */
    public function updateAppSettings(AppSettingRequest $request)
    {
        if(Auth::user()->role != 'admin') {
            return response::json(['code' => 405]);
        }
        AppSetting::where('tag', 'deviceId')->update(array('value' => $request['app_setting_deviceId']));
        AppSetting::where('tag', 'password')->update(array('value' => $request['app_setting_password']));
        AppSetting::where('tag', 'email')->update(array('value' => $request['app_setting_email']));

        return response::json(['code' => 200]);
    }

    /**
     * Change the user password.
     *
     * @param ChangePasswordRequest $request
     * @return mixed
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user 	= User::findorfail(Auth::user()->id)->first();
        $user->update(array("password" => hash::make($request['password'])));

        return Response::json(['code' => 200 ]);       
    }

    /**
     * Check user is admin.
     *
     * @return boolean
     */

    public function isAdmin() 
    {
        return Auth::user()->role == 'admin';
    }
}
