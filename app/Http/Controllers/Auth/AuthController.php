<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\AuthRequest;
use App\Profile;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    protected $redirectPath = '/dashboard';
    protected $loginPath = '/';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout','activate']]);
    }

    /**
     *  Display the login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return View('auth.login');
    }

    /**
     * Handle the user login request.
     *
     * @param AuthRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(AuthRequest $request)
    {
        $user = User::where('email', $request->email)
                    ->where('active', 0)->first();
        if($user) {
            return redirect()->back()->with('error_login', 'Please activate your account first by clicking the link that was sent to your email.');
        }
        // Try to login the user using its email and password.
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Credentials are valid. User will be redirected to dashboard page.
            return redirect('/dashboard');
        }

        // Login failed.
        return redirect()->back()->with('error_login', 'Invalid Username or Password!');
    }

    /**
     * Handle the user logout request.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    /**
     * Handle the activation process of the user when link on the
     * email is clicked.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function activate(Request $request)
    {
        // Check if activation code is present.
        if(!$request->code) {
            return redirect('/');
        }

        // Get the non active user based on the activation code.
        $user = User::where('activation_code', $request->code)->where('active', 0)->first();
        if(!$user) {
            return redirect('/');
        }

        // set the user status to active.
        $user->active = 1;
        $user->activation_code = '';

        // Create a new user profile.
        if($user->save()){
            $profile = new Profile();
            $profile->user_id = $user->id;
            if($profile->save()) {
                Session::flush();
                Auth::login($user);
                Session::flash('account_activated', 'Account Activated.');
                return redirect('dashboard');
            }
        }
    }
}
