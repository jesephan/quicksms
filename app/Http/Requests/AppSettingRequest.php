<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AppSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_setting_email' => 'required',
            'app_setting_deviceId' => 'required',
            'app_setting_password' => 'required',
        ];
    }
}
