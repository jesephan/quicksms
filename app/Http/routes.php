<?php


Route::get('/', 'Auth\AuthController@getLogin')->middleware(['guest']);
Route::get('logout', 'Auth\AuthController@getLogout');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('auth/activate', 'Auth\AuthController@activate');
Route::group(['middleware' => 'auth'], function(){
	Route::get('dashboard', 'ProfileController@dashboard');
	Route::post('profile/invite', 'ProfileController@invite');
	Route::post('profile/update', 'ProfileController@update');
	Route::post('profile/delete', 'ProfileController@delete');
	Route::get('profile/getContacts', 'ProfileController@getContacts');
	Route::post('profile/send', 'ProfileController@send');
	Route::post('profile/updateAppSettings', 'ProfileController@updateAppSettings');
	Route::post('profile/changePassword', 'ProfileController@changePassword');
});
