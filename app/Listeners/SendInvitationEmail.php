<?php

namespace App\Listeners;

use App\Events\UserWasInvited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendInvitationEmail
{
     /**
     * Handle the event.
     *
     * @param  UserWasInvited  $event
     * @return void
     */
    public function handle(UserWasInvited $event)
    {
        $request = $event->data;
        Mail::send('emails.invite_new_user', ['data' => $request], function ($m) use ($request) {
            $m->from( 'admin@quicksms.com', 'QuickSMS');

            $m->to($request['email'])->subject('You are invited to join QuickSMS!');
        });
    }
}
