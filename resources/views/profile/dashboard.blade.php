@extends('layout')

@section('content')
    @include('partials.sms_form')
    @include('partials.contacts_modal')
    @include('partials.change_password')
    @include('partials.edit_profile_modal')
    @if(Auth::check() && Auth::user()->role == 'admin')
        @include('partials.app_setting_modal')
        @include('partials.invite_form')
    @endif
@endsection