@extends('layout')

@section('content')
    <div class="login-form">
        <div class="form-wrapper">
            @include('errors.lists')
            {!! Form::open(array('action' => 'Auth\AuthController@postLogin', 'method' => 'post')) !!}
                <div class="form-group">
                    <div class="input-group login">
                        <span class="input-group-addon"><span><i class="fa fa-user"></i></span></span>
                        <input type="email" name="email" required class="form-control" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group login">
                        <span class="input-group-addon"><span><i class="fa fa-lock"></i></span></span>
                        <input type="password" name="password" required class="form-control" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group login">
                        <button type="submit" class="btn btn-default">Login</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection