@extends('layout')

@section('content')    
    @include('errors.lists')
    <div class="row">    
        {{Form::open(array('action' => 'AuthController@store', 'method' => 'post', 'class' => 'form-horizontal col-md-offset-2'))}}
            <div class="form-group">
                <label for="email" class="control-label col-xs-2">Firstname*</label>
                <div class="col-xs-5">
                    <input type="text" class="form-control" id="firstname" required value="{{ Input::old('firstname') }}" name="firstname" placeholder="Firstname">
                </div>
            </div>
        <div class="form-group">
                <label for="email" class="control-label col-xs-2">Lastname*</label>
                <div class="col-xs-5">
                    <input type="text" class="form-control" id="lastname" required value="{{ Input::old('lastname') }}" name="lastname" placeholder="Lastname">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-xs-2">Email*</label>
                <div class="col-xs-5">
                    <input type="email" class="form-control" id="email" required value="{{ Input::old('email') }}" name="email" placeholder="Email">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="control-label col-xs-2">Password*</label>
                <div class="col-xs-5">
                <input type="password" class="form-control" id="password" required name="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="confirmpassword" class="control-label col-xs-2">Confirm Password*</label>
                <div class="col-xs-5">
                <input type="password" class="form-control" id="confirmpassword" required name="password_confirmation" placeholder="Confirm Password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-offset-2 col-xs-5">
                    <button type="submit" class="btn btn-primary">Register</button>
                    &nbsp;&nbsp;&nbsp;or &nbsp;&nbsp;<span><a href="{{url('/')}}">Login</a></span>
                </div>
            </div>
        {{Form::close()}}
    </div>
@endsection