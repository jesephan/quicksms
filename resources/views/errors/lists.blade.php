@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (Session::has('error'))
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            <li>{{ Session::get('error') }}</li>
        </ul>
    </div>
@endif
@if (Session::has('error_login'))
    <div class="alert alert-danger">
        <strong>Whoops!</strong> {{ Session::get('error_login') }}
    </div>
@endif

@if (Session::has('session_expired'))
    <div class="alert alert-danger">
        {!! Session::get('session_expired') !!}

    </div>
@endif