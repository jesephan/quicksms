<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="token" content="{{csrf_token()}}">
    <title>QuickSMS</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="output/final.css">    

</head>
<body>
<nav role="navigation" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">QuickSMS</a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            {{--<ul class="nav navbar-nav ">--}}
                {{--<li><a href="{{url('/')}}">Home</a></li>--}}
            {{--</ul>--}}
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    @if(Auth::user()->role == 'admin')
                        <li><a href="#invite-new-user" data-toggle="modal" >Invite New User</a></li>
                    @endif
                    <li><a href="#contact-list" data-toggle="modal">Contacts</a></li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">{{Auth::user()->email}} <b class="caret"></b></a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="#edit-profile" data-toggle="modal" >Edit Profile</a></li>
                            <li><a href="#change-password" data-toggle="modal" >Change Password</a></li>
                            @if(Auth::user()->role == 'admin')
                            <li><a href="#app-settings" data-toggle="modal" >Edit App Settings</a></li>
                            @endif
                            <li class="divider"></li>
                            <li><a href="{{url('/logout')}}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<section class="content">
    <div class="container">
        @yield('content')
    </div>
</section>
<div id="contact-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
</div>
<script src="output/final.js"></script>
@if(Session::has('account_activated'))
    <script>
        $('#edit-profile').modal('show');
    </script>
@endif
</body>
</html>                                		