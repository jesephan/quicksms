<div id="contact-list" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Contacts</h4>
            </div>
            <div class="modal-body">
                <div id="modal-error-msg"></div>
                <div id="modal-form">
                    @if(Auth::check() && Auth::user()->role == 'admin')
                        @include('partials.admin_edit_profile')
                    @endif
                </div>
                <div class="contact-table">
                    <table class="table table-condensed" id="contacts-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone #</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="contact-list-items">
                        @if(!$contacts->isEmpty())
                            @foreach($contacts as $contact)
                                <tr id="row-{{$contact->id}}">
                                    <td class="contact-name">{{$contact->name}}</td>
                                    <td class="contact-phone-number">{{$contact->phone_number}}</td>
                                    <td>
                                        <a href="#" class="send-sms" data-sms="{{$contact->id}}"><i class="fa fa-envelope"></i></a>&nbsp;&nbsp;
                                        @if(Auth::check() && Auth::user()->role == 'admin')
                                            <a href="#" class="edit-contact" data-edit="{{$contact->id}}"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                                            <a href="#" class="delete-contact" data-delete="{{$contact->user_id}}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>