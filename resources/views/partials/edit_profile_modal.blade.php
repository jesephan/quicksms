<div id="edit-profile" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Profile</h4>
            </div>
            <div class="modal-body">
                <div id="edit-profile-modal-error-msg"></div>
                <div id="modal-form">
                    <form class="form-vertical" id="edit-profile-form">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" value="{{$current_user_profile->name}}" id="edit-profile-name" name="name" placeholder="Name">
                            {{csrf_field()}}
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control" value="{{$current_user_profile->phone_number}}" name="phone_number" maxlength="13" id="edit-profile-phone-number" placeholder="Phone #: ex +639499384877">
                        </div>
                        <button type="submit" id="edit-profile-btn" class="btn btn-primary">Update Profile</button>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>