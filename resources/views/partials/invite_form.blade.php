<div id="invite-new-user" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Invite New User</h4>
            </div>
            <div class="modal-body">
                <div id="admin-invite-contact-modal-error-msg"></div>
                <div id="modal-form">
                    <form class="form-vertical" id="invite-new-user-form">
                        <div class="form-group">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" id="invite-email" name="email" placeholder="Email">
                            {{csrf_field()}}
                        </div>
                        <button type="submit" id="invite-contact-btn" class="btn btn-primary">Invite User</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>