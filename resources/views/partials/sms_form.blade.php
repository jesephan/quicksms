<div class="sms-form">
    <div class="form-wrapper">
        <div id="sms-error-msg"></div>
        <form id="send-sms-form">
            {{csrf_field()}}
            <div class="form-group">
                <div class="input-group login">
                    <select id="receiver" placeholder="Select an SMS receiver(s)" name='receiver[]' multiple="multiple">
                        @if(!$contacts->isEmpty())
                            @foreach($contacts as $contact)
                                <option value="{{$contact->id}}">{{$contact->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group login">
                    <textarea cols="20", name="message" id="message" rows="10" class="form-control" placeholder="Message"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group login">
                    <button type="submit" id="send-sms" class="btn btn-success send-sms-btn">Send SMS</button>
                </div>
            </div>
        </form>
    </div>
</div>