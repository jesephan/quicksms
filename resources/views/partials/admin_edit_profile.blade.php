<div id="admin-edit-contact-modal-error-msg"></div>
<form class="form-vertical" id="edit-contact-form" style="display:none;">
    <div class="form-group">
        <input type="hidden" name="id" value="" id="admin-edit-contact-id">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="admin-edit-name" name="name" placeholder="Name">
        {{csrf_field()}}
    </div>
    <div class="form-group">
        <label for="phone_number">Phone Number</label>
        <input type="text" class="form-control" name="phone_number" maxlength="13" id="admin-edit-phone-number" placeholder="Phone #: ex +639499384877">
    </div>
    <button type="submit" id="admin-edit-contact-btn" class="btn btn-primary">Update Profile</button>
</form>