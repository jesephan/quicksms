<div id="change-password" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <div id="change-password-modal-error-msg"></div>
                <div id="modal-form">
                    <form class="form-vertical" id="change-password-form">
                        <div class="form-group">
                            <label for="new_password">New Password</label>
                            <input type="password" class="form-control" value="" name="password" id="new-password" placeholder="New Password">
                            {{csrf_field()}}
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password</label>
                            <input type="password" class="form-control" value="" name="password_confirmation" id="confirm-new-password" placeholder="Confirm Password">
                        </div>
                        <button type="submit" id="change-password-btn" class="btn btn-primary">Change Password</button>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>