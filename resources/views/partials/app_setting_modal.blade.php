<div id="app-settings" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit App Settings</h4>
            </div>
            <div class="modal-body">
                <div id="app-settings-modal-error-msg"></div>
                <div id="modal-form">
                    <form class="form-vertical" id="app-settings-form">
                        <div class="form-group">
                            <label for="app_setting_email">SMS Gateway Email</label>
                            <input type="text" class="form-control" value="{{$app_setting_email}}" id="app_setting_email" name="app_setting_email" placeholder="Email">
                            {{csrf_field()}}
                        </div>
                        <div class="form-group">
                            <label for="app_setting_deviceId">SMS Gateway Device ID or API Key</label>
                            <input type="text" class="form-control" value="{{$app_setting_deviceId}}" id="app_setting_deviceId" name="app_setting_deviceId" placeholder="Device ID">
                        </div>
                        <div class="form-group">
                            <label for="app_setting_password">SMS Gateway Hash Value</label>
                            <input type="text" class="form-control" value="{{$app_setting_password}}" name="app_setting_password" id="app_setting_password" placeholder="Password">
                        </div>
                        <button type="submit" id="edit-app-settings-btn" class="btn btn-primary">Update App Settings</button>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>