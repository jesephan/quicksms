<p>Hi,</p><br/>
<p>You are invited to join QuickSMS.</p>
<p>To activate your account, please click this <a href="{{url('auth/activate?code='.$data['code'])}}">link</a>.</p>
<p>Your temporary password is {{$data['password']}}</p><br/>
<p>Regards,</p><br/>
<p>QuickSMS Admin</p>